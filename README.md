# Komputer Store

<p>This is small project made to simulate a computer store which allows user to raise money by <br> 
'working' which allows the user to buy one of the computers listed in the 'shop'.</p>

## Features: 3 main parts

<p>The user can 'work' by clicking the work button. This button generates 100 NOK which can be <br>
transfered to you bank account.</p>

<p>The bank is balance what the user can spend on computers from the store. The bank also allows <br> 
the user to get a loan. The user can only have one loan the time and loans can be repaid with a <br>
'pay loan' button.</p>

<p>The last part is the laptops which the user can buy. The user can select computers from a dropdown<br> 
menu. 
Name, specs, description and price is listed and the user can buy the computer by clicking the<br>  
buy button. The user can successfully purchase a computer if the bank balance is sufficient</p>

## Langues and framework
<p>The application is built using html, javascript and bootstrap.<br>

<p> Requires node server setup to run. Which is added in a zip </p>
