import { fetchComputers } from './api.js'

class App {
    constructor() {
        // DOM elements
        this.elStatus = document.getElementById('app-status')
        this.elComputerSelect = document.getElementById('computers')
        this.elSelectedComputer = document.getElementById('selected-computer')
        // Card elements: computer information
        this.elComputerName = document.getElementById('computer-title') // revurder id navnet
        this.elComputerDesc = document.getElementById('computer-description')
        this.elComputerPrice = document.getElementById('computer-price')
        this.elComputerImg = document.getElementById('img-computer')
        this.btnBuyComputer = document.getElementById('btn-buy-computer')
        this.btnBuyComputer.style.display = "none"; // this has to be reworked
        // Feature element
        this.elFeaturesTitle = document.getElementById('features-title')
        this.elComputerFeatures = document.getElementById('computer-features')
        // Bank elements
        this.elBankBalance = document.getElementById('bank-balance')
        this.elOutstandingLoanTitle = document.getElementById('outstanding-loan-title')
        this.elOutstandingLoan = document.getElementById('outstanding-loan')
        this.btnPayLoan = document.getElementById('btn-pay-loan')
        this.btnPayLoan.style.display = "none"; // this has to be reworked
        this.btnApplyLoan = document.getElementById('btn-apply-loan')
        // Work elements
        this.elPayBalance = document.getElementById('pay-balance')
        this.btnTransferPay = document.getElementById('btn-transfer-pay')
        this.btnWork = document.getElementById('btn-work')

        // Properties
        this.payBalance = 0
        this.bankBalance = 0
        this.loanSum = 0
        this.hasLoan = false
        this.computers = []
        this.featList = []
        this.selectedComputer = null
    }

    onComputerChange() {
        // to avoid code to execute if no computer is selected
        if (parseInt(this.elComputerSelect) === -1) {
            return
        }

        this.selectedComputer = this.computers.find(computer => {
            // parse because the value is a string
            return computer.id === parseInt(this.elComputerSelect.value)
        })

        this.displayComputer()
    }

    displayComputer() {
        // if user picks select computer option
        if (this.elComputerSelect.value == -1) {
            this.elComputerName.innerText = 'Please select a computer'
            this.elComputerDesc.innerText = ''
            this.elComputerPrice.innerText = ''
            this.elComputerImg.src = ''
            this.elComputerFeatures.innerText = ''
            this.btnBuyComputer.style.display = "none"
            this.elFeaturesTitle.innerText = ''
        } else {
            // Display computer in card
            this.elComputerName.innerText = this.selectedComputer.name
            this.elComputerDesc.innerText = this.selectedComputer.description
            this.elComputerPrice.innerText = this.selectedComputer.price + ' NOK'
            this.elComputerImg.src = this.selectedComputer.image
            this.btnBuyComputer.style.display = "inline-block"
            // Change if you return to this project:
            this.elFeaturesTitle.innerText = 'Features:'
            this.featList = this.selectedComputer.features // array with features
            let string = ''
            for (let i = 0; i < this.featList.length; i++) {
                string += this.featList[i] + '\n'
            }

            this.elComputerFeatures.innerText = string
        }
    }

    setInitialBalance() {
        this.elPayBalance.innerText = this.payBalance + ' NOK'
        this.elBankBalance.innerText = this.bankBalance + ' NOK'
    }

    executeWork() {
        // Increases pay balance when you work
        this.payBalance += 100
        this.elPayBalance.innerText = this.payBalance + ' NOK'
    }

    transferPayToBank() {
        const subtractPayBalanceTest = this.loanSum - this.payBalance
        // bank paybalance 
        if (this.hasLoan === false) {
            this.bankBalance += this.payBalance
            this.payBalance = 0

        } else {
            // 10 % to pay loan
            this.loanSum = this.loanSum - this.payBalance * 0.10
            this.elOutstandingLoan.innerText = this.loanSum + ' NOK'
            // Rest to the bank
            this.bankBalance = this.bankBalance + this.payBalance * 0.90
            this.payBalance = 0
            // Checks if the loan is below or 0 to change hasLoan to false and to make sure rest of balance stays in pay balance
            if (this.loanSum <= 0) {
                this.payBalance = Math.abs(subtractPayBalanceTest)
                this.hasLoan = false
                this.loanSum = 0
                this.btnPayLoan.style.display = 'none'
                this.btnApplyLoan.style.display = 'inline-block'
                this.elOutstandingLoanTitle.innerText = ''
                this.elOutstandingLoan.innerText = ''
            }
        }

        this.elPayBalance.innerText = this.payBalance + ' NOK'
        this.elBankBalance.innerText = this.bankBalance + ' NOK'
    }

    payLoan() {
        const subtractPayBalance = this.loanSum - this.payBalance

        if (subtractPayBalance <= 0) {
            this.payBalance = Math.abs(subtractPayBalance)
            this.loanSum = 0
            this.hasLoan = false

            this.btnPayLoan.style.display = 'none'
            this.btnApplyLoan.style.display = 'inline-block'
            this.elOutstandingLoanTitle.innerText = ''
            this.elOutstandingLoan.innerText = ''
        } else {
            this.loanSum = this.loanSum - this.payBalance
            this.payBalance = 0
            this.elOutstandingLoan.innerText = this.loanSum + " NOK"
        }

        this.elPayBalance.innerText = this.payBalance + ' NOK'

    }

    applyForLoan() {
        const maxLoanSum = this.bankBalance * 2;
        const inputLoanSum = Number(window.prompt("Type the amount you want to loan. \nMax loan can only be double the amount of you bank balance ", ""));
        let loanMessage = ''

        const regexPromptInput = /^([0-9]{1,10})$/.test(inputLoanSum)

        if (regexPromptInput === true && inputLoanSum <= maxLoanSum && inputLoanSum != 0) {
            this.bankBalance = this.bankBalance + inputLoanSum // note to self: this.inputLoanSum returns NaN
            this.elBankBalance.innerText = this.bankBalance + ' NOK'
            this.loanSum = inputLoanSum
            this.hasLoan = true

            this.btnPayLoan.style.display = 'inline-block'
            this.btnApplyLoan.style.display = 'none'
            this.elOutstandingLoanTitle.innerText = 'Outstanding loan'
            this.elOutstandingLoan.innerText = this.loanSum + ' NOK'

            loanMessage = 'Loan was accepted'
        } else if (inputLoanSum > maxLoanSum) {
            loanMessage = 'Loan sum was too high'
        } else {
            loanMessage = 'Invalid input'
        }

        alert(loanMessage)
    }

    buyComputer() {
        if (this.bankBalance >= this.selectedComputer.price) {

            this.bankBalance = this.bankBalance - this.selectedComputer.price

            this.elBankBalance.innerText = this.bankBalance + ' NOK'
            alert('You are now the owner of the new laptop!')
        } else {
            alert('insufficient balance!')
        }
    }

    startEventListener() {
        this.elComputerSelect.addEventListener('change', this.onComputerChange.bind(this))
        this.btnTransferPay.addEventListener('click', this.transferPayToBank.bind(this))
        this.btnBuyComputer.addEventListener('click', this.buyComputer.bind(this))
        this.btnWork.addEventListener('click', this.executeWork.bind(this))
        this.btnApplyLoan.addEventListener('click', this.applyForLoan.bind(this))
        this.btnPayLoan.addEventListener('click', this.payLoan.bind(this))
    }

    async init() {
        // Event listeners
        // bind is needed when you're working with classes and functional constructors
        this.setInitialBalance()

        this.startEventListener()

        this.elStatus.innerText = 'Loading computers...'
        this.elComputerSelect.disabled = true // select cant ble clicked while computers are loading

        try {
            this.computers = await fetchComputers()
            this.elStatus.innerText = ' '
        } catch (e) {
            this.elStatus.innerText = 'Error! ' + e.message
        } finally {
            this.elComputerSelect.disabled = false
        }

        this.render()
    }

    render() {
        const elComputer = document.createElement('option')
        elComputer.innerText = '---Select a computer---'
        elComputer.value = -1
        this.elComputerSelect.appendChild(elComputer)

        this.computers.forEach(computer => {
            const elComputer = document.createElement('option')
            elComputer.innerText = computer.name
            elComputer.value = computer.id
            this.elComputerSelect.appendChild(elComputer)
        })
    }
}

new App().init()